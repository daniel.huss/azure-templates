# Azure templates

## Deploying

You can deploy the templates directly through the [Azure Portal](https://portal.azure.com) or by using the script supplied in the root of the repo.

To deploy a template using the Azure Portal, click the **Deploy to Azure** button found in the README.md of each folder.

To deploy the template via the command line using [Azure PowerShell](https://docs.microsoft.com/en-us/powershell/azure/overview) you can use the scripts below.

Simply execute the script and pass in the folder name of the sample you want to deploy.  

For example:

### PowerShell
```PowerShell
.\Deploy-AzureTemplate.ps1 qlik-sense-single-node
```

## Files, folders and naming conventions

Every deployment template and its associated files are contained in its own **folder**.

A **README.md** file explains how the template works.

The deployment template file is named **azuredeploy.json**.

There should be a parameters file named **azuredeploy.parameters.json**. 
 + Fill out the values for the parameters according to rules defined in the template (allowed values etc.)