# Qlik Sense Enterprise - Single node

This template will deploy a new Virtual Machine and will configure it with Qlik Sense Enterprise.

Click the button below to deploy:

<a href="https://portal.azure.com/#create/Microsoft.Template/uri/https%3A%2F%2Fgitlab.com%2Fdaniel.huss%2Fazure-templates%2Fraw%2Fmaster%2Fqlik-sense-single-node%2Fazuredeploy.json" target="_blank">
    <img src="http://azuredeploy.net/deploybutton.png"/>
</a>

#### The deployment can take up to 15 minutes.
